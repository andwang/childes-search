//window.persist_index = 0;
window.sections = document.getElementsByClassName("persist-area");
window.sectionHeaders = document.getElementsByClassName("persist-header");

function updateHeaders() {
    //var scrollTop = document.body.scrollTop;
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;  // i hate the internet
    var showThis;

    /*
    var prev = window.sections[window.persist_index-1];
    var cur = window.sections[window.persist_index];
    var next = window.sections[window.persist_index+1];

    if (prev && scrollTop > prev.offsetTop && scrollTop < prev.offsetTop + prev.clientHeight) {
        //showThis = prev;
        showThis = window.sectionHeaders[--window.persist_index];
        console.log("show prev");
    } else if (cur && scrollTop > cur.offsetTop && scrollTop < cur.offsetTop + cur.clientHeight) {
        //showThis = cur;
        showThis = window.sectionHeaders[window.persist_index];
        //console.log("no change needed", showThis);
        return; // no change needed
    } else if (next && scrollTop > next.offsetTop && scrollTop < next.offsetTop + next.clientHeight) {
        //showThis = next;
        showThis = window.sectionHeaders[++window.persist_index];
        console.log("show next");
    } else {
        console.log("no header applicable");
        return;
    }
    */
    var loop_broke = false;
    var i;
    for (i = 0; i < window.sections.length; ++i) {
        var elem = window.sections[i];
        if (elem && scrollTop > elem.offsetTop && scrollTop < elem.offsetTop + elem.clientHeight) {
            showThis = window.sectionHeaders[i];
            loop_broke = true;
            break;
        }
    }
    if (!loop_broke)
        return;  // no header applicable
    var topHeader = document.getElementById("top-header");
    if (topHeader.className == i)
        return; // no change needed
    showThis = showThis.cloneNode(true);
    showThis.className = '';  // don't trick the selector into thinking this is a real persistent header
    console.log("Clearing", topHeader);
    topHeader.innerHTML = '';  // clear children
    topHeader.appendChild(showThis);  // show this one instead
    topHeader.className = i;
    console.log("topHeader is", showThis);
}

function savePage(searchQuery) {
    console.log("saving pages to");
    console.log(targetNode);
    var lines = document.getElementsByClassName("matched-line");
    var csvLines = [['file', 'lineno', 'speaker', 'match-start', 'match-end', 'utterance']];
    for (var i = 0; i < lines.length; ++i) {
        var line;
        var utterance, deps;
        line = lines[i];
        utterance = line.children[0];  // p
        deps = line.children[1];  // div

        var matchedWhere = utterance.children[0];
        var file = matchedWhere.getElementsByClassName("file")[0].innerHTML;
        var lineno = matchedWhere.getElementsByClassName("lineno")[0].innerHTML;

        var lineContent = utterance.children[1];  // span
        var speaker = lineContent.getElementsByClassName("line-key")[0].innerHTML.substr(1);
        var actualUtterance = lineContent.getElementsByClassName("content")[0];
        var renderedLineContent = actualUtterance.textContent;
        var matchLineContent = actualUtterance.innerHTML;

        // calculate match
        var m1, m2;
        m1 = matchLineContent.indexOf("<b>") ;
        m2 = matchLineContent.indexOf("</b>") - 3;  // subtract 3 because of the opening tag

        row = [file, lineno, speaker, m1, m2, renderedLineContent].map(function(s) { return csvEncode(typeof s === "string"?s:JSON.stringify(s)); });
        csvLines.push(row.join(','));
    }
    var payload = btoa(csvLines.join('\n'));
    var targetNode = document.getElementById("download-button");
    targetNode.download = "childes-search " + searchQuery + " results.csv";
    targetNode.href = "data:text/csv;base64," + payload;
    targetNode.innerHTML = "Download now";
    targetNode.click();
}

function csvEncode(s) {
    if (s.indexOf(",") !== -1 || s.indexOf('"') !== -1) {
        s = s.replace(/"/g, '""');
        s = ['"', '"'].join(s);
    }
    return s;
}
