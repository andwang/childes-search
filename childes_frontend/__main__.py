from gevent import monkey
monkey.patch_all()

import json
from functools import wraps
from collections import OrderedDict
import gsocketpool.pool
from mprpc import RPCPoolClient
from flask import Flask, Blueprint, url_for, request, render_template, Response, current_app


DEBUG = True
DEBUG = False

HOME_TPL = "/childes-search/"

SUPPORTED_LANGUAGES = {"English": 'en', "Spanish": 'es', "Eng-NA": 'en-na', "Eng-UK": 'en-uk', "German": 'de', "French": 'fr', "Russian": 'ru', "Polish": 'pl', "Croatian": 'hr', "Serbian": 'sr', "Slovenian": 'sl', "Japanese": 'ja'}

#client_pool = gsocketpool.pool.Pool(RPCPoolClient, {"host": '127.0.0.1', "port": 54321, 'unpack_params': {'use_list': False, 'object_pairs_hook': OrderedDict}})
client_pool = gsocketpool.pool.Pool(RPCPoolClient, {"host": '127.0.0.1', "port": 54321})

def refresh_conns(func):
    # Yes I know this defeats the purpose of connection pooling
    @wraps(func)
    def wrapped(*a, **kw):
        with client_pool.connection() as client:
            client.reconnect()
            if not client.is_connected():
               client_pool.drop(client)
        return func(*a, **kw)
    return wrapped

# based on https://tedboy.github.io/flask/patterns/streaming.html
def stream_template(template_name, **context):
    real_app.update_template_context(context)
    t = real_app.jinja_env.get_template(template_name)
    res = t.stream(context)
    res.enable_buffering(3)
    return res

if DEBUG:
    stream_template = render_template


def deploy(base_flask, language, language_short):
    app = Blueprint("devserver-{}".format(language_short), __name__)
    HOME = HOME_TPL + language_short

    @app.route("/")
    @refresh_conns
    def index():
        with client_pool.connection() as client:
            return Response(stream_template("index.html", HOME=HOME, **client.call("index", language)))


    @app.route("/donate")
    def donate():
        return Response(stream_template("donate.html", HOME=HOME))


    @app.route("/view/<string:corpus>/<path:path>")
    @app.route("/view//<string:corpus>/<path:path>")  # fuck you
    @refresh_conns
    def view(corpus, path):
        with client_pool.connection() as client:
            return Response(stream_template("view.html", HOME=HOME, conversation=client.call("view", language, corpus, path)))


    @app.route("/search", methods=["POST"])
    @refresh_conns
    def search():
        search_these = request.form.getlist("corpus")

        uttregex = request.form.get("uttregex") or None
        uttcontain = request.form.get("uttcontain") or None

        depregex = request.form.get("depregex") or None
        depcontain = request.form.get("depcontain") or None

        context = request.form.get("context")
        try:
            context = int(context)
        except ValueError:
            context = 0

        name = request.form.get("name") or " " * 4

        age_lo, _, age_hi = request.form.get("age").partition("-")
        try:
            age_lo = float(age_lo)
        except ValueError:
            age_lo = None
        try:
            age_hi = float(age_hi)
        except ValueError:
            age_hi = None

        args = {
                "uttre": uttregex,
                "uttcontain": uttcontain,
                "depre": depregex,
                "depcontain": depcontain,
                "context": context,
                "name": name,
                "agerange": (age_lo, age_hi),
            }
        with client_pool.connection() as client:
            return Response(stream_template("search.html", HOME=HOME, **client.call("search", language, search_these, args)))

    base_flask.register_blueprint(app, url_prefix=HOME)

real_app = Flask(__name__)

# First deploy the home index
@real_app.route(HOME_TPL)
@refresh_conns
def index():
    return Response(stream_template("base-index.html", languages=SUPPORTED_LANGUAGES))

for language, language_short in SUPPORTED_LANGUAGES.items():
    print("Deploying", language, "CHILDES")
    deploy(real_app, language, language_short)
real_app.run(port=8000, debug=DEBUG)
