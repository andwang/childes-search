// a bunch of utility functions for parsing Talkbank .cha files
// http://talkbank.org/manuals/CHAT.pdf documentation
// TODO we should probably spin this off into an independent library

use std;
use std::clone::Clone;
use std::fmt::Debug;
use std::iter::FromIterator;
use std::collections::{VecDeque, HashMap};
use regex::Regex;

mod contexts {
    pub const FILE_BEGIN: u8 = 1 << 0;
    pub const UTTERANCE: u8 = 1 << 1;
    pub const FILE_END: u8 = 1 << 2;
}

pub mod sigils {
    pub const HEADER: char = '@';
    pub const DEP: char = '%';
    pub const SPEECH: char = '*';
}

pub const max_len: usize = 4;
pub const NUL: u8 = b' ';

#[derive(Debug)]
pub enum Line {
    Utterance {
        speaker: [u8; max_len],
        content: String,
        deps: Vec<Line>
    },
    Dependent {
        name: [u8; max_len],
        content: String
    },
    BeginHeader,
    /*ParticipantsHeader {
        code: [u8; max_len],
        name: String,
        role: String
        //is_target: bool  // instead of actually recording the role that probably doesn't matter
    },*/
    IdHeader {
        language: Option<String>,  // ISO code, e.g. en
        corpus: Option<String>,  // corpus name
        code: Option<[u8; max_len]>,  // three-letter name for the speaker
        age: Option<f32>,  // y;m.d
        sex: Option<String>,  // male|female
        // dgaf below
        group: Option<String>,
        ethnicity: Option<String>,
        role: Option<String>,
        education: Option<String>,
        custom: Option<String>,

        content: String,
    },
    Header {
        name: String,
        content: Option<String>
    },
    EndHeader,
    Empty
}

impl std::fmt::Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self) // trolololol
    }
}

#[derive(Debug)]
pub struct Participant {
    pub code: [u8; max_len],  // three-letter name for the speaker
    pub age: Option<f32>,
    pub age_display: Option<String>,  // y;m.d
    pub sex: Option<String>,  // male|female
    pub ethnicity: Option<String>,
    pub role: Option<String>,
}

pub struct Chat {
    pub participants: HashMap<[u8; max_len], Participant>,
    pub conversation: Box<[(usize, Line)]>
}

pub struct ChatParser {
    // hopefully we'll never ever ever need to backtrack further than one line
    context: u8,
    stack: VecDeque<(usize, Line)>,
    lineno: usize,
    line: String,
    parsed_line: Line,
    cursor: usize,
    participants: HashMap<[u8; max_len], Participant>,
    conversation: Vec<(usize, Line)>
}

pub fn parse<T: std::io::BufRead>(io: T) -> Chat {
    let mut parser = ChatParser::new();
    for line in io.lines() {
        if let Ok(line) = line {
            parser.feed(&line);
        }
    }
    parser.participants.shrink_to_fit();
    parser.conversation.shrink_to_fit();
    Chat {
        participants: parser.participants,
        conversation: parser.conversation.into_boxed_slice()
    }
}

impl ChatParser {
    pub fn new() -> ChatParser {
        ChatParser {
            context: 0,
            lineno: 0,
            line: String::new(),
            stack: VecDeque::new(),
            parsed_line: Line::Empty,
            cursor: 0,
            participants: HashMap::new(),
            conversation: Vec::new()
        }
    }

    pub fn read(&mut self, delta: isize) -> char {
        let cur = self.cursor;
        if delta < 0 {
            let delta = (-1 * delta) as usize;
            if cur - delta < 0 {
                '\0'
            } else {
                self.line.chars().nth(cur - delta).unwrap()
            }
        } else {
            // 0 <= delta
            let delta = delta as usize;
            if cur + delta > self.line.len() {
                '\0'
            } else {
                self.line.chars().nth(cur + delta).unwrap()
            }
        }
    }

    pub fn colon_partition<T: Iterator<Item=char>>(&mut self, linechars: &mut T) -> (String, bool) {
        // returns the stuff up to (but not including) the colon or the whole
        // line, as well as whether it found a colon. The colon itself is not
        // returned.
        let mut tok: VecDeque<char> = VecDeque::new();
        let mut found_colon = false;
        while let Some(ch) = linechars.next() {
            self.cursor += 1;
            if ch == ':' {
                found_colon = true;
                break;
            }
            tok.push_back(ch);
        }
        (String::from_iter(tok), found_colon)
    }

    pub fn feed(&mut self, line: &str) {
        self.line = String::from(line);
        self.lineno += 1;
        let mut buf = line.chars();
        if let Some(ch) = buf.next() {
            self.cursor += 1;
            let cb: fn(&mut Self, &mut _) = match ch {
                // $ find /opt/corpora/ | grep '.cha$' | xargs -d '\n' cat | perl -ne 'print substr $_, 0, 1; print "\n";' | sort | uniq
                sigils::HEADER => Self::parse_header,
                sigils::DEP => Self::parse_dep,
                sigils::SPEECH => Self::parse_speech,
                c if c.is_whitespace() => Self::parse_blank,
                x => unreachable!("didn't expect {}", x)
            };
            cb(self, &mut buf);
        }
    }

    fn parse_header<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        // read until : or new line
        let (name, found_colon) = self.colon_partition(rest);
        if name == "Begin" {
            self.parse_begin_header(rest)
        } else if name == "ID" {
            let content = String::from_iter(rest).trim_left().to_string();
            let (line, speaker) = {
                let (language, corpus, code, age, age_display, sex, group, ethnicity, role, education, custom) = {
                    let mut parts = content.split("|").map(|s| s.to_string());
                    let language = parts.next();
                    let corpus = parts.next();
                    let code = parts.next().map(|s| {
                        let mut real_code = [NUL; max_len];
                        for (real_ch, ch) in real_code.iter_mut().zip(s.as_bytes()) {
                            *real_ch = *ch;
                        }
                        real_code
                    });  // the three-letter code for the speaker in capitals
                    let age_display = parts.next().filter(|s| s.len() > 0); let age = age_display.as_ref().map(|s| {
                        let age_regex = Regex::new(";|\\.").unwrap();
                        let mut components: Vec<f32> = age_regex.splitn(&s, 3).map(|i| i.parse().unwrap_or(0.0)).collect();
                        while components.len() < 3 {
                            components.push(0.0);
                        }
                        1.0 * components[0] + (1.0/12.0) * components[1] + (1.0/365.0) * components[2]
                    }).filter(|i| *i > 0.0);  // the age of the speaker
                    let sex = parts.next();  // the sex of the speaker
                    let group = parts.next();
                    let ethnicity = parts.next();  // WC working class, UC upper class, MC middle class, LI limited income "If Ethnicity is not listed, it is assumed to be White" :(
                    let role = parts.next();
                    let education = parts.next();  // Elem, HS, UG, Grad, Doc
                    let custom = parts.next();
                    let _end = parts.next();
                    (language, corpus, code, age, age_display, sex, group, ethnicity, role, education, custom)
                };
                (Line::IdHeader {
                    language,
                    corpus,
                    code: code.clone(),
                    age: age.clone(),
                    sex: sex.clone(),
                    group, ethnicity: ethnicity.clone(), role: role.clone(), education, custom,
                    content
                }, Participant {
                    code: code.clone().unwrap(),
                    age,
                    age_display,
                    sex: sex.clone(),
                    ethnicity: ethnicity.clone(), role: role.clone(),
                })
            };
            self.participants.insert(speaker.code.clone(), speaker);
            self.conversation.push((self.lineno, line));
        } else if name == "End" {
            if self.context & contexts::UTTERANCE != 0 {
                let last = self.stack.pop_front().unwrap();
                self.context ^= contexts::UTTERANCE;
                self.conversation.push(last);
            }
            self.parse_end_header(rest)
        } else {
            let content = if found_colon {
                Some(String::from_iter(rest).trim_left().to_string())
            } else {
                None
            };
            if self.context & contexts::UTTERANCE != 0 {
                // TODO lol idk
                self.conversation.push((self.lineno, Line::Empty));
            } else {
                self.conversation.push((self.lineno, Line::Header { name, content }));
            }
        }
    }

    fn parse_dep<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        if self.context & contexts::UTTERANCE == 0 {
            unreachable!();
        }
        let (dep, found_colon) = self.colon_partition(rest);
        if !found_colon {
            unreachable!();
        }
        let dep_content = String::from_iter(rest).trim_left().to_string();
        let mut a = [NUL as u8; max_len];  // TODO this'll fuck up utf8 like hell
        for (a_ch, ch) in a.iter_mut().zip(dep.as_bytes()) {
            *a_ch = *ch;
        }
        let (_, ref mut uttered) = *self.stack.front_mut().unwrap();
        if let Line::Utterance { ref mut deps, .. } = *uttered {
            (*deps).push(Line::Dependent { name: a, content: dep_content });
        }
    }

    fn parse_speech<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        let (speaker, found_colon) = self.colon_partition(rest);
        if !found_colon {
            unreachable!();
        }
        let content = String::from_iter(rest).trim_left().to_string();
        let mut a = [NUL as u8; max_len];  // TODO this'll fuck up utf8 like hell
        for (a_ch, ch) in a.iter_mut().zip(speaker.as_bytes()) {
            *a_ch = *ch;
        }
        if self.context & contexts::UTTERANCE != 0 {
            self.context ^= contexts::UTTERANCE;
            self.conversation.push(self.stack.pop_front().unwrap());
        }
        self.context |= contexts::UTTERANCE;
        self.stack.push_front((self.lineno, Line::Utterance { speaker: a, content, deps: Vec::new() }));
        //ret
    }

    fn parse_blank<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        // TODO this is really sloppy but hey it works
        // ideally we'd just parse each line into a linear list of Lines
        // and then have another function that builds them into a conversation
        // until then it's this shit
        let prev = self.stack.front_mut();
        let content = String::from_iter(rest).trim_left().to_string();
        //println!("Parsing blank: {:?}", content);
        match prev {
            Some(prev) => {
                match *prev {
                    (_, Line::Utterance { content: ref mut linecontent, ref mut deps, .. }) => {
                        let mut real_line = if let Some(&mut Line::Dependent { content: ref mut depcontent, .. }) = deps.last_mut() {
                            depcontent
                        } else {
                            linecontent
                        };
                        real_line.push(' ');
                        real_line.push_str(&content);
                    },
                    (_, _) => { /* hope the next generation thinks of something better */ }
                }
            },
            None => {}
        }
    }

    // line-internal parsers

    fn parse_begin_header<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        if self.context & contexts::FILE_BEGIN == 0 {
            self.context |= contexts::FILE_BEGIN;
            if rest.next() != None {
                unreachable!();
            } else {
                self.conversation.push((self.lineno, Line::BeginHeader));
            }
        } else {
            unreachable!();
        }
    }

    fn parse_end_header<T: Iterator<Item=char>>(&mut self, rest: &mut T) {
        if self.context & contexts::FILE_BEGIN == 0 {
            unreachable!();
        } else {
            self.context ^= contexts::FILE_BEGIN;
            if rest.next() != None {
                unreachable!();
            } else {
                self.conversation.push((self.lineno, Line::EndHeader));
            }
        }
    }
}
