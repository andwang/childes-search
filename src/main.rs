#![feature(range_contains)]
#![feature(exclusive_range_pattern)]
#![feature(const_transmute)]
#![feature(extern_prelude)]

extern crate flexi_logger;
extern crate iter_read;
extern crate regex;
extern crate simple_parallel;
extern crate futures;
extern crate rmp_rpc;
extern crate rmp_serde;
extern crate rmpv;
extern crate tokio;
#[macro_use] extern crate log;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate serde_derive;

extern crate childes_search;


use std::io::{BufReader, Write};
use std::io;
use std::ffi::{CStr, CString};
use std::fs::File;
use std::iter::FilterMap;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::collections::{HashMap, HashSet};
use std::ops::Range;
use std::sync::{Arc, Mutex};
use std::cell::Cell;
use std::net::SocketAddr;

use flexi_logger::Logger;
use iter_read::IterRead;
use regex::Regex;
use simple_parallel::pool;
use futures::{future, Future, Poll, Stream};
use rmp_rpc::{serve, Service, Value};
use rmp_serde::{Deserializer, Serializer};
use tokio::net::TcpListener;

use childes_search::childes;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct FuckYouMatch<'t> {
    text: &'t str,
    start: usize,
    end: usize,
}

// fuck u
const ALWAYS_MATCH: regex::Match<'static> = unsafe { std::mem::transmute::<_, regex::Match<'static>>(FuckYouMatch { text: "", start: 0, end: 0 }) };

pub type CorpusHolder = HashMap<PathBuf, HashMap<PathBuf, HashMap<PathBuf, childes::Chat>>>;
pub struct Server;

#[derive(Deserialize, Debug)]
pub struct CorpusSearcherArgs {
    pub uttre: Option<String>,
    pub uttcontain: Option<String>,

    pub context: usize,

    pub depre: Option<String>,
    pub depcontain: Option<String>,

    pub name: String,
    pub agerange: (Option<f32>, Option<f32>),
}

pub struct CorpusSearcher {
    pub uttre: Option<Regex>,
    pub uttre_i: Option<Regex>,

    pub context: usize,

    pub depre: Option<Regex>,
    pub depre_i: Option<Regex>,

    pub name: [u8; childes::max_len],
    pub agerange: Option<Range<f32>>,
}

impl From<CorpusSearcherArgs> for CorpusSearcher {

    fn from(CorpusSearcherArgs { uttre, uttcontain, context, depre, depcontain, name: name_, agerange }: CorpusSearcherArgs) -> CorpusSearcher {
        let uttregex = uttre.filter(|x| x.trim() != "");
        let uttcontain = uttcontain.filter(|x| x.trim() != "").map(|x| regex::escape(&x));

        let context = context;

        let depregex = depre.filter(|x| x.trim() != "");
        let depcontain = depcontain.filter(|x| x.trim() != "").map(|x| regex::escape(&x));

        let mut name = [childes::NUL as u8; childes::max_len];
        name.iter_mut().zip(name_.as_bytes()).for_each(|(speaker_ch, ch)| *speaker_ch = *ch);
        let agerange = match agerange {
                (Some(lo), Some(hi)) => Some(lo .. hi),
                (Some(lo), None) => Some(lo .. 999.0),
                (None, Some(hi)) => Some(0.0 .. hi),
                (None, None) => None,
            };
        /*let agerange = Some(v).filter(|x| x.trim() != "").map(|v| {
            let i = v.find('-').unwrap_or(v.len());
            let (age_lo, age_hi) = v.split_at(i);
            let age_lo = age_lo.parse().unwrap_or(0.0);
            /* the leading dash remains so the upper bound will
             * be parsed as a negative - we don't want that.
             * If we skip the dash, we run the risk of allowing
             * negative input "1--5", so flip the sign around */
            let age_hi = -age_hi.parse().unwrap_or(-999.0);
            age_lo .. age_hi //Range { start: age_lo, end: age_hi }
        });*/
        let uttre = match (uttregex, uttcontain) {
                (Some(r), Some(s)) => Regex::new(&format!("({}|{})", r, s)).ok(),
                (None, None) => None,
                (r, s) => r.or(s).map_or(None, |stuff| Regex::new(&stuff).ok()),
            };
        let uttre_i = uttre.as_ref().map_or(None, |s| Regex::new(&format!("(?i){}", &s)).ok());

        info!("computed utterance regex: {:?}", uttre);
        let depre = match (depregex, depcontain) {
                (Some(r), Some(s)) => Regex::new(&format!("({}|{})", r, s)).ok(),
                (None, None) => None,
                (r, s) => r.or(s).map_or(None, |stuff| Regex::new(&stuff).ok()),
            };
        let depre_i = depre.as_ref().map_or(None, |s| Regex::new(&format!("(?i){}", &s)).ok());
        info!("computed dependent tier regex: {:?}", depre);
        CorpusSearcher { uttre, uttre_i, name, context, depre, depre_i, agerange }
    }
}

pub struct SearchParams(Vec<PathBuf>, CorpusSearcher);
/*
impl<'a>  SearchParams {

    fn from_form(items: &mut FormItems<'a>, _strict: bool) -> Result<SearchParams, String> {
        let mut corpora: Vec<PathBuf> = Vec::new();

        let (mut uttregex, mut uttcontain) = (None, None);
        let mut context: usize = 0;

        let (mut depregex, mut depcontain) = (None, None);

        let mut name = [childes::NUL as u8; childes::max_len];
        let mut agerange = None;

        debug!("{:?}", items.inner_str());
        for (k, v) in items {
            if let Ok(v) = v.url_decode() {
                match k.as_str() {
                    "corpus" => corpora.push(PathBuf::from(v)),

                    "uttregex" => uttregex = Some(v).filter(|x| x.trim() != ""),
                    "uttcontain" => uttcontain = Some(v).filter(|x| x.trim() != "").map(|x| regex::escape(&x)),

                    "context" => context = v.parse().unwrap_or(0),

                    "depregex" => depregex = Some(v).filter(|x| x.trim() != ""),
                    "depcontain" => depcontain = Some(v).filter(|x| x.trim() != "").map(|x| regex::escape(&x)),

                    "name" => name.iter_mut().zip(v.as_bytes()).for_each(|(speaker_ch, ch)| *speaker_ch = *ch),
                    "age" => agerange = Some(v).filter(|x| x.trim() != "").map(|v| {
                        let i = v.find('-').unwrap_or(v.len());
                        let (age_lo, age_hi) = v.split_at(i);
                        let age_lo = age_lo.parse().unwrap_or(0.0);
                        /* the leading dash remains so the upper bound will
                         * be parsed as a negative - we don't want that.
                         * If we skip the dash, we run the risk of allowing
                         * negative input "1--5", so flip the sign around */
                        let age_hi = -age_hi.parse().unwrap_or(-999.0);
                        age_lo .. age_hi //Range { start: age_lo, end: age_hi }
                    }),
                    "sex" => (),  // TODO actually do this bit
                    //_ if strict => return Err("sad".to_owned()),
                    _ => {}
                }
            }
        }
        let uttre = Regex::new(
            & match (uttregex, uttcontain) {
                (Some(r), Some(s)) => format!("({}|{})", r, s),
                (None, None) => panic!("no search query"),
                (r, s) => r.or(s).unwrap(),
            }).unwrap();
        let uttre_i = Regex::new(&format!("(?i){}", &uttre)).unwrap();
        info!("computed utterance regex: {:?}", uttre);
        let depre = Regex::new(
            & match (depregex, depcontain) {
                (Some(r), Some(s)) => format!("({}|{})", r, s),
                (None, None) => String::new(),
                (r, s) => r.or(s).unwrap(),
            }).unwrap();
        let depre_i = Regex::new(&format!("(?i){}", &depre)).unwrap();
        info!("computed dependent tier regex: {:?}", depre);
        Ok(SearchParams(corpora, CorpusSearcher { uttre, uttre_i, name, context, depre, depre_i, agerange }))
    }
}
*/
/*
#[derive(Serialize,Debug)]
struct AtomicSearchLine<'a> {
    line: &'a childes::Line,
    matches_at: (usize, usize)
}
*/

#[derive(Debug)]
struct SearchLine<'a> {
    line: &'a childes::Line,
    matches_at: (usize, usize),
    context_before: Vec<(usize, SearchLine<'a>)>,
    context_after: Vec<(usize, SearchLine<'a>)>,
}

#[derive(Serialize, Debug)]
pub struct HtmlSearchLine<'a> {
    color: &'a str,
    sigil: char,
    content: (&'a str, &'a str, &'a str),
    key: &'a str,
    context_before: Vec<(usize, HtmlSearchLine<'a>)>,
    context_after: Vec<(usize, HtmlSearchLine<'a>)>,
    deps: Box<[HtmlSearchLine<'a>]>  // will be NULL if this is itself a dep
}

#[derive(Serialize, Debug)]
struct SearchSummary {
    utt_regex_str: Option<String>,
    dep_regex_str: Option<String>,
    agerange: Option<(f32, f32)>,
    cum_corpora_matches: usize,
    cum_convo_matches: usize,
    cum_num_matches: usize
}

#[derive(Serialize)]
#[serde(remote = "childes::Participant")]
struct ParticipantDef {
    pub code: [u8; childes::max_len],  // three-letter name for the speaker
    pub age: Option<f32>,
    pub age_display: Option<String>,  // y;m.d
    pub sex: Option<String>,  // male|female
    pub ethnicity: Option<String>,
    pub role: Option<String>,
}


#[derive(Serialize, Debug)]
pub struct HtmlChat<'a> {
    #[serde(serialize_with="ser_participants_hashmap")]
    participants: &'a HashMap<[u8; childes::max_len], childes::Participant>,
    conversation: Box<[(usize, HtmlSearchLine<'a>)]>
}

// b o i l e r p l a t e https://github.com/serde-rs/serde/issues/1369 maybe std::mem::transmute instead?
fn ser_participants_hashmap<S>(shit: &HashMap<[u8; childes::max_len], childes::Participant>, serializer: S) -> Result<S::Ok, S::Error> where S: serde::Serializer {
    #[derive(Serialize)]
    struct Wrapper<'a>(#[serde(with = "ParticipantDef")] &'a childes::Participant);
    let map = shit.iter().map(|(k, v)| (std::str::from_utf8(k).unwrap(), Wrapper(v)));
    serializer.collect_map(map)
}

#[derive(Serialize, Debug)]
pub struct SearchResults<'a> {
    search_results: HashMap<PathBuf, HashMap<PathBuf, HtmlChat<'a>>>,
    summary: SearchSummary
}

impl<'a> SearchLine<'a> {

    // shorthand to produce a SearchLine without context 
    fn atomic(matches_at: (usize, usize), line: &'a childes::Line) -> SearchLine<'a> {
        SearchLine {
            matches_at,
            line,
            context_before: vec!(),
            context_after: vec!()
        }
    }


    fn compress((lineno, mut line): (usize, SearchLine<'a>)) -> (Range<usize>, Range<usize>, Vec<(usize, SearchLine<'a>)>) {
        let before_size = line.context_before.len();
        let after_size = line.context_after.len();
        let mut line_compressed = Vec::with_capacity(before_size + after_size + 1);
        let mut line_before = std::mem::replace(&mut line.context_before, vec!());
        let mut line_after = std::mem::replace(&mut line.context_after, vec!());
        line_compressed.append(&mut line_before);
        line_compressed.push((lineno, line));
        line_compressed.append(&mut line_after);
        (0..before_size, before_size+1..before_size+after_size+1, line_compressed)
    }


    fn decompress(before_range: Range<usize>, after_range: Range<usize>, mut compressed_line: Vec<(usize, SearchLine<'a>)>) -> (usize, SearchLine<'a>) {
        let context_after = compressed_line.drain(after_range).collect::<Vec<_>>();  // end goes first because it won't change any indices
        let context_before = compressed_line.drain(before_range).collect::<Vec<_>>();  // all that's left now is the match line
        let mut match_line = compressed_line.pop().unwrap();
        assert!(compressed_line.is_empty());
        match_line.1.context_before = context_before;
        match_line.1.context_after = context_after;
        match_line
    }

}


impl<'a> From<SearchLine<'a>> for HtmlSearchLine<'a> {

    fn from(SearchLine { line, matches_at: (start, end), context_before, context_after }: SearchLine<'a>) -> Self {
        let context_before = context_before.into_iter().map(|(lineno, line)| (lineno, HtmlSearchLine::from(line))).collect();
        let context_after = context_after.into_iter().map(|(lineno, line)| (lineno, HtmlSearchLine::from(line))).collect();
        match *line {
            childes::Line::Utterance { ref speaker, ref content, ref deps } => {
                let (left, (highlight, right)) = match content.split_at(start) {
                    (left, mid) => (left, mid.split_at(end - start))
                };
                let mut htmldeps = Vec::with_capacity(deps.len());
                for d in deps {
                    htmldeps.push(HtmlSearchLine::from(SearchLine::atomic((0, 0), d)));
                }
                HtmlSearchLine {
                    sigil: childes::sigils::SPEECH,
                    color: "#008000",
                    key: std::str::from_utf8(speaker).unwrap(),
                    content: (left, highlight, right),
                    context_before,
                    context_after,
                    deps: htmldeps.into_boxed_slice()
                }
            },
            childes::Line::Dependent { ref name, ref content } => {
                let content = ("", "", content.as_str());
                HtmlSearchLine {
                    sigil: childes::sigils::DEP,
                    color: "#800080",
                    key: std::str::from_utf8(name).unwrap(),
                    content,
                    context_before,
                    context_after,
                    deps: Box::new([])
                }
            },
            childes::Line::BeginHeader => HtmlSearchLine {
                sigil: childes::sigils::HEADER,
                color: "#800808",
                key: "Begin",
                content: ("", "", ""),
                context_before,
                context_after,
                deps: Box::new([])
            },
            childes::Line::Header { ref name, ref content } => {
                //let content = ("", "", if let Some(ref content) = *content { content } else { "" });
                //let content: () = content.as_ref().map(String::as_str);
                let content = ("", "", content.as_ref().map(String::as_str).unwrap_or(""));
                HtmlSearchLine {
                    sigil: childes::sigils::HEADER,
                    color: "#800808",
                    key: name,
                    content,
                    context_before,
                    context_after,
                    deps: Box::new([])
                }
            },
            childes::Line::EndHeader => HtmlSearchLine {
                sigil: childes::sigils::HEADER,
                color: "#800808",
                key: "End",
                content: ("", "", ""),
                context_before,
                context_after,
                deps: Box::new([])
            },
            childes::Line::IdHeader { ref content, .. } => HtmlSearchLine {
                sigil: childes::sigils::HEADER,
                color: "#800808",
                key: "ID",
                content: ("", "", content),
                context_before,
                context_after,
                deps: Box::new([])
            },
            childes::Line::Empty => unreachable!()
        }
    }
}


impl CorpusSearcher {

    fn search<'a>(&self, chat: &'a childes::Chat) -> Vec<(usize, SearchLine<'a>)> {
        let mut ret = Vec::new();
        //println!("age range = {:?}, speakers = {:?}", self.agerange, chat.participants);
        for (i, &(lineno, ref line)) in chat.conversation.iter().enumerate() {
            match *line {
                childes::Line::Utterance { ref speaker, ref content, ref deps } => {
                    let speaker = &chat.participants[speaker];

                    let speaker_okay = self.name == speaker.code || self.name == [childes::NUL; childes::max_len];
                    //let age_okay = self.agerange == None || (speaker.age && self.agerange.contains(speaker.age));
                    let age_okay = self.agerange.as_ref().map(
                        |r| {
                            speaker.age.as_ref().map(
                                |age| {
                                    r.contains(age)
                                }).unwrap_or(false)
                        }).unwrap_or(true);
                    let utterance_match = self.uttre_i.as_ref().map_or(Some(ALWAYS_MATCH), |r| r.find(content));
                    let deps_match = deps.iter().any(
                        |line| {
                            if let childes::Line::Dependent { ref content, .. } = *line {
                                self.depre_i.as_ref().map_or(true, |r| r.find(content).is_some())
                            } else {
                                false
                            }
                        });

                    let should_add = speaker_okay && age_okay && utterance_match.is_some() && deps_match;

                    if should_add {
                        let matches_at = utterance_match.map(|m| (m.start(), m.end())).unwrap();
                        // TODO this doesn't highlight the dependent search matches
                        let mut context_before = Vec::with_capacity(self.context);
                        let mut context_after = Vec::with_capacity(self.context);
                        for j in (1..self.context+1).rev() {
                            if (i as isize) - (j as isize) < 0 {
                                break;
                            }
                            let printable = match chat.conversation[i-j].1 {
                                childes::Line::Utterance { .. } | childes::Line::Dependent { .. } => true,
                                _ => false
                            };
                            if printable {
                                context_before.push((chat.conversation[i-j].0, SearchLine::atomic((0, 0), &chat.conversation[i-j].1)));
                            }
                        }
                        for j in 1..self.context+1 {
                            if i + j >= chat.conversation.len() {
                                break;
                            }
                            let printable = match chat.conversation[i+j].1 {
                                childes::Line::Utterance { .. } | childes::Line::Dependent { .. } => true,
                                _ => false
                            };
                            if printable {
                                context_after.push((chat.conversation[i+j].0, SearchLine::atomic((0, 0), &chat.conversation[i+j].1)));
                            }
                        }
                        ret.push((lineno, SearchLine { matches_at, line, context_before, context_after }));
                    }
                },
                childes::Line::Dependent { .. } => unreachable!(),
                childes::Line::BeginHeader | childes::Line::Header { .. } | childes::Line::IdHeader { .. } | childes::Line::EndHeader | childes::Line::Empty => {}
            }
        }
        ret
    }
}

#[derive(Serialize, Debug)]
pub struct IndexContainer<'a> {
    dirs: Vec<&'a str>,
    speaker_max_len: usize
}

const BASE_PATH: &str = "/opt/corpora";

pub fn index<'a>(corpora: &'a CorpusHolder, language: PathBuf) -> IndexContainer<'a> {
    let mut dirs: Vec<&str> = corpora[&language].keys().map(|pb| pb.to_str().unwrap_or("<invalid corpus>")).collect();
    dirs.sort_unstable();
    let speaker_max_len = childes::max_len;
    IndexContainer { dirs, speaker_max_len }
}

pub fn view<'a>(corpora: &'a CorpusHolder, language: PathBuf, corpus: PathBuf, path: PathBuf) -> HashMap<String, HashMap<String, HtmlChat<'a>>> {
    for k in corpora.keys() { println!("example key {:?}", k); println!("example value's key: {:?}", corpora[k].keys().next()); break; }
    let rest: PathBuf = PathBuf::from(BASE_PATH).join(&language).join(&corpus).join(&path);
    println!("corpus: {:?}, other thing: {:?}", corpus, rest);
    let chat = &corpora[&language][&corpus][&rest];
    //let chat = load_corpus(&PathBuf::from(BASE_PATH).join(&path));
    //let mut results: HashMap<PathBuf, HashMap<PathBuf, Box<[(usize, _)]>>> = HashMap::new();
    //let mut every_line: Vec<(usize, _)> = Vec::new();

    // okay i copy pasted this but i have a fucking deadline so fight me fatass
    // WEEHAWKEN
    // DAWN
    // GUNS
    // DRAWN
    //let matches = CorpusSearcher::new().search(chat);
    let matches = chat.conversation.iter().filter(
        |&&(_lineno, ref line)| {
            match *line {
                childes::Line::Empty => false,
                _ => true
            }
        }).map(|&(lineno, ref line)| (lineno, SearchLine::atomic((0, 0), line)));
    //let matches_len = matches.len();
    //if matches_len > 0 { println!("found a match in {:?} in file {:?}", c, cha_file); }
    //println!("old capacity: {}, old len: {}", matches.capacity(), matches.len());
    let printable_matches = matches
        // merge consecutive headers into one box
        .fold(Vec::new(), |mut acc: Vec<(usize, SearchLine)>, (lineno, SearchLine { matches_at, line, context_before, context_after })| {
            let mut merge_headers = false;
            if let Some(mut prev) = acc.last_mut() {
                match (prev.1).line {
                    childes::Line::BeginHeader | childes::Line::Header { .. } | childes::Line::IdHeader { .. } | childes::Line::EndHeader => match line {
                        childes::Line::BeginHeader | childes::Line::Header { .. } | childes::Line::IdHeader { .. } | childes::Line::EndHeader => prev.1.context_after.push((lineno, SearchLine::atomic((0, 0), line))),
                        _ => merge_headers = true
                    },
                    _ => merge_headers = true
                }
            } else {
                merge_headers = true;
            }
            if merge_headers {
                acc.push((lineno, SearchLine { matches_at, line, context_before, context_after }));
            }
            acc
        }).into_iter()
        .map(|(lineno, line)| (lineno, HtmlSearchLine::from(line))).collect::<Vec<_>>();
        // turn into line structs
        /*.map(|(lineno, SearchLine { matches_at, line, context_before, context_after })| {
            //println!("{} - {:?}", lineno, line);
            let mut linestruct = HtmlSearchLine::from(matches_at, line);
            let context_before_linestructs = context_before.into_iter().map(|(lineno, line)| {
                (lineno, HtmlSearchLine::from((0, 0), line))
            }).collect::<Vec<_>>();
            let context_after_linestructs = context_after.into_iter().map(|(lineno, line)| {
                (lineno, HtmlSearchLine::from((0, 0), line))
            }).collect::<Vec<_>>();
            linestruct.context_before = context_before_linestructs;
            linestruct.context_after = context_after_linestructs;
            (lineno, linestruct)
        }).collect::<Vec<_>>();*/
    let every_line = printable_matches;
    let corpus = PathBuf::from(path.into_iter().next().unwrap());
    let mut results: HashMap<String, HashMap<String, HtmlChat>> = HashMap::new();
    results.insert(String::from(corpus.to_str().unwrap()), vec![(String::from(path.to_str().unwrap()), HtmlChat { participants: &chat.participants, conversation: every_line.into_boxed_slice() })].into_iter().collect());
    results
}


pub fn search<'a>(corpora: &'a CorpusHolder, language: PathBuf, params: SearchParams) -> SearchResults<'a> {
    let SearchParams(selected_corpora, searcher) = params;
    let mut results: HashMap<PathBuf, HashMap<PathBuf, HtmlChat>> = HashMap::new();
    let search_these = {
        // e.g. /opt/corpora/English/(Cornell|Halls|Blackwell|...)
        let mut ret = format!("{}/{}/", BASE_PATH, language.display());
        if !selected_corpora.is_empty() {
            ret.push('(');
            for corpus in &selected_corpora {
                ret.push_str(corpus.to_str().unwrap());
                ret.push('|');
            }
            ret.pop();  // get rid of the trailing pipe
            ret.push(')');  // and close the regex group
        }
        ret
    };
    println!("searching here: {}", search_these);
    // the magic happens here!
    // TODO this shit actually needs tests
    let mut outputs = [
            // csearch utterances
            searcher.uttre.as_ref().map(|uttre|
                Command::new("csearch")
                    .arg("-l")
                    .arg("-i")
                    .args(&["-f", &search_these])
                    .arg(uttre.as_str())
                    .output()
                    .unwrap()
                ),

            // csearch dependent tiers
            searcher.depre.as_ref().map(|depre|
                Command::new("csearch")
                    .arg("-l")
                    .arg("-i")
                    .args(&["-f", &search_these])
                    .arg(depre.as_str())
                    .output()
                    .unwrap()
                ),

            // convos including people in the age range
            if let &Some(Range { start: lo, end: hi }) = &searcher.agerange {
                Command::new("./age-search.py")
                    .arg(format!("{}", lo))
                    .arg(format!("{}", hi))
                    .output().ok()
            } else {
                None
            },
        ];
    let realsets: Vec<_> = outputs.iter_mut()
        .filter_map(|mut set| set.take())
        .collect();
    let realrealsets = realsets.iter()
        .map(|output| -> HashSet<&str> {
            std::str::from_utf8(&output.stdout)
                .unwrap()
                .trim()
                .split('\n')  // one match per line
                .filter(|line| line.trim().len() > 0)
                .collect()
        });
    // adapted from https://www.reddit.com/r/rust/comments/5v35l6/intersection_of_more_than_two_sets/ddz06ho/
    let mut iter = realrealsets.into_iter();
    let intersection = iter.next().map(|set| iter.fold(set, |set1, set2| set1.intersection(&set2).map(|i| *i).collect::<HashSet<_>>())).unwrap_or_else(HashSet::new);

    let matched_files = intersection
        .into_iter()
        .map(PathBuf::from)
        .filter(|p| p.extension().map(|ext| ext == "cha").unwrap_or(false))  // only get cha files
        .filter_map(|pb| pb.strip_prefix(BASE_PATH).and_then(|pb| pb.strip_prefix(&language)).map(PathBuf::from).ok());  //  get relative paths

    //println!("will search {:?}", matched_files);

    let mut match_map: HashMap<PathBuf, Vec<PathBuf>> = matched_files.fold(
        HashMap::new(),
        |mut acc, cha_file| {
            let corpus = cha_file.iter().next().map(PathBuf::from);
            match corpus {
                Some(corpus) => if selected_corpora.contains(&corpus) {
                    acc.entry(corpus).or_insert(vec!()).push(cha_file);
                },
                None => {},
            }
            acc
        });
    for c in selected_corpora {
        if let Some(convos) = match_map.remove(&c) {
            let convo_matches: Arc<Mutex<HashMap<PathBuf, HtmlChat>>> = Arc::new(Mutex::new(HashMap::new()));
            let mut workers = pool::Pool::new(8);
            workers.for_(convos.into_iter(), |cha_file| {
                println!("trying to search {:?}", c);
                //let convo = load_corpus(&PathBuf::from(BASE_PATH).join(&cha_file));
                let convo = &corpora[&language].get(&c).and_then(|corp| corp.get(&PathBuf::from(BASE_PATH).join(&language).join(&cha_file)));
                let matches = convo.map_or_else(Vec::new, |conv| searcher.search(&conv));
                let matches_len = matches.len();
                if matches_len > 0 { println!("found a match in {:?} in file {:?}", c, cha_file); }
                //println!("old capacity: {}, old len: {}", matches.capacity(), matches.len());
                let printable_matches: Vec<(usize, HtmlSearchLine)> = matches.into_iter()
                    //.collect::<Vec<_>>();
                    .fold(Vec::with_capacity(matches_len), |mut acc: Vec<(usize, SearchLine)>, mut cur_line: (usize, SearchLine)| {
                        // make it so that windows don't overlap
                        // <       O    [  >  @     ] (start)
                        // will be turned into
                        // <       O       >[ @     ] (phase 1)
                        // then turned into
                        // <       O          @     > (phase 2)
                        // ??????????????????
                        // this doesn't get past the fucking borrow checker even though it works (nightly-2017-08-something)
                        if let Some(mut prev_line) = acc.pop() {
                            let mut i = 0;
                            let mut any_overlap = false;
                            // if the current matched line appears in the previous line's context, highlight it
                            for &mut (prev_lineno, ref mut prev_context_line) in prev_line.1.context_after.iter_mut() {
                                if prev_lineno == cur_line.0 {
                                    prev_context_line.matches_at = cur_line.1.matches_at;
                                }
                            }
                            let (cur_context_before_range, cur_context_after_range, mut cur_line_compressed) = SearchLine::compress(cur_line);
                            if let Some(&(prev_right_num, _)) = prev_line.1.context_after.last() {
                                for (i_, &(cur_left_num, _)) in cur_line_compressed.iter().enumerate() {
                                    if cur_left_num <= prev_right_num {
                                        // if the context windows overlap then we know there are duplicates.
                                        // we need to know at what point we stop seeing duplicates (phase 1)
                                        i = i_;
                                        any_overlap = true;
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if any_overlap {
                                // initiate phase 2
                                cur_line_compressed.drain(..i+1);
                                prev_line.1.context_after.append(&mut cur_line_compressed);
                                acc.push(prev_line);
                            } else {
                                cur_line = SearchLine::decompress(cur_context_before_range, cur_context_after_range, cur_line_compressed);
                                acc.push(prev_line);
                                acc.push(cur_line);
                            }
                        } else {
                            acc.push(cur_line);
                        }
                        acc
                    }).into_iter()
                    /*.map(|(lineno, SearchLine { matches_at, line, context_before, context_after })| {
                        //println!("{} - {:?}", lineno, line);
                        let mut linestruct = HtmlSearchLine::from(matches_at, &line);
                        let context_before_linestructs = context_before.into_iter().map(|(lineno, line)| {
                            (lineno, HtmlSearchLine::from(line))
                        }).collect::<Vec<_>>();
                        let context_after_linestructs = context_after.into_iter().map(|(lineno, line)| {
                            (lineno, HtmlSearchLine::from(line))
                        }).collect::<Vec<_>>();
                        linestruct.context_before = context_before_linestructs;
                        linestruct.context_after = context_after_linestructs;
                        (lineno, linestruct)
                    })*/
                    .map(|(i, line)| (i, HtmlSearchLine::from(line))).collect();
                /*let mut printable_matches = Vec::with_capacity(matches.len());
                println!("before capacity {}", printable_matches.capacity());
                for (lineno, (start, end), line) in matches {
                    // tera templates kinda suck so we'll just have to make do
                    //let linestr = serde_json::to_string(&line).unwrap();
                    //let lineval: Value = serde_json::from_str(&linestr).unwrap();
                    let linestruct: HtmlSearchLine = HtmlSearchLine::from(&line, (start, end));
                    //println!("{}", linestruct.color);
                    //println!("{}", linestr);
                    //convo_matches.entry(cha_file).or_insert(Vec::with_capacity(matches.len())).push((lineno, linestruct));
                    printable_matches.push((lineno, linestruct));
                }*/
                //println!("new capacity: {}, new len: {}", printable_matches.capacity(), printable_matches.len());
                if !printable_matches.is_empty() {
                    convo_matches.lock().unwrap().insert(cha_file, HtmlChat { participants: &convo.unwrap().participants, conversation: printable_matches.into_boxed_slice() });
                }
            });
            if !convo_matches.lock().unwrap().is_empty() {
                results.insert(c, Arc::try_unwrap(convo_matches).unwrap().into_inner().unwrap());
            }
        } else {
            // um some sort of injection
        }
    }
    let cum_corpora_matches = results.values().filter(|ref v| v.values().map(|matches| matches.conversation.len()).sum::<usize>() > 0).collect::<Vec<_>>().len();
    let cum_convo_matches = results.values().map(|ref v| v.keys().len()).sum();
    // results: HashMap<PathBuf, HashMap<PathBuf, Box<[(usize, _)]>>>
    let cum_num_matches =
        results.values().map(
            |v|
                v.values().map(
                    |v_|
                        v_.conversation.iter().map(
                            |(_, utt_box)|
                                utt_box.context_before.iter().filter(|(_, line)| (line.content.0, line.content.1) != ("", "")).count()
                                +
                                ((utt_box.content.0, utt_box.content.1) != ("", "")) as usize  // TODO wrong way to calculate, use should_add instead
                                +
                                utt_box.context_after.iter().filter(|(_, line)| (line.content.0, line.content.1) != ("", "")).count()
                        ).sum::<usize>()
                ).sum::<usize>()
        ).sum::<usize>();
/*
        for v in results.values():
            v: HashMap<PathBuf, Box<[(usize, OwnedSearchLine)]>>
            for v_ in v.values():
                v_: Box<[(usize, OwnedSearchLine)]>
                for (_, utt_box) in v_:
                    utt_box: OwnedSearchLine
                    utt_box.context_before.iter().filter(|(_, line)| (line.content.0, line.content.1) != ("", "")).count()
                    +
                    utt_box.context_after.iter().filter(|(_, line)| (line.content.0, line.content.1) != ("", "")).count()
*/
    let data = SearchResults {
        search_results: results,
        summary: SearchSummary {
            utt_regex_str: searcher.uttre.as_ref().map(|r| format!("{}", r)),
            dep_regex_str: searcher.depre.as_ref().map(|r| format!("{}", r)),
            agerange: searcher.agerange.as_ref().map(|r| (r.start, r.end)),
            cum_corpora_matches,
            cum_convo_matches,
            cum_num_matches
        }
    };
    data
}

fn load_corpus(cha: &PathBuf) -> childes::Chat {
    let f = match File::open(cha) {
        Ok(f) => BufReader::new(f),
        Err(_) => panic!("couldn't read corpus at {:?}", cha)
    };
    childes::parse(f)
}

pub fn load_corpora() -> CorpusHolder {
    warn!("load_corpora() uses a metric fuckton of RAM");
    let mut ret: HashMap<PathBuf, HashMap<PathBuf, HashMap<PathBuf, childes::Chat>>> = HashMap::new();
    let p = Path::new(BASE_PATH);  // BASE_PATH/language/corpus/maybe/dirs/convo.cha
    if let Ok(languages) = p.read_dir() {
        let languages = languages
            /* First filter out all the reads that didn't work
             * You should probably be doing a match here and printing a warning
             * or something
             */
            .filter_map(|d| d.ok())
            .map(|d| d.path())
            .filter(|p| p.is_dir());  // We only care about directories
        for lang_full_path in languages {
            if let Ok(corpora) = lang_full_path.read_dir() {
                let language = lang_full_path.strip_prefix(BASE_PATH).unwrap().to_path_buf();
                let corpora = corpora
                    /* First filter out all the reads that didn't work
                     * You should probably be doing a match here and printing a warning
                     * or something
                     */
                    .filter_map(|d| d.ok())
                    .map(|d| d.path())
                    .filter(|p| p.is_dir());  // We only care about directories
                let mut workers = pool::Pool::new(8);
                for corpus in corpora {
                    let mut convos = vec!();
                    let mut search_stack = vec!();
                    search_stack.push(corpus.clone());
                    while !search_stack.is_empty() {
                        let here = search_stack.pop().unwrap();
                        if let Ok(children) = here.read_dir() {
                            for child in children.filter_map(|child| child.ok()) {
                                search_stack.push(child.path());
                                //break;
                            }
                        } else if here.is_file() && here.extension().map_or(None, |s| s.to_str()) == Some("cha") {
                            convos.push(here);
                        }
                    }
                    println!("lang {:?}, corpus {:?}", language, corpus);
                    let corpus = corpus.strip_prefix(BASE_PATH).and_then(|pb| pb.strip_prefix(&language)).unwrap().to_path_buf();
                    let processed_convos = Mutex::new(Cell::new(HashMap::new()));
                    workers.for_(convos, |convo| {
                        println!("Loading {:?}", convo);
                        let chat = load_corpus(&convo);
                        processed_convos.lock().unwrap().get_mut().insert(convo, chat);
                    });
                    ret.entry(language.to_path_buf()).or_insert_with(HashMap::new).insert(corpus, processed_convos.into_inner().unwrap().into_inner());
                }
            }
        }
    }
    ret
}

lazy_static! {
    static ref ALL_CORPORA: CorpusHolder = load_corpora();
}

fn struct_to_val<T: Sized + serde::ser::Serialize>(shit: T) -> Value {
    let mut buf: Vec<u8> = rmp_serde::encode::to_vec_named(&shit).unwrap();
    rmpv::decode::read_value(&mut buf.as_slice()).unwrap()  // idk why you need the cursor
}

fn val_to_struct<'a, T: Sized + serde::de::DeserializeOwned>(val: &'a Value) -> T {
    let mut buf = vec!();
    rmpv::encode::write_value(&mut buf, val).unwrap();
    let res = rmp_serde::decode::from_read(&buf[..]);
    res.unwrap()
}

impl Service for Server {
    type RequestFuture = Box<Future<Item = Value, Error = Value> + Send>;
    fn handle_request(&mut self, method: &str, params: &[Value]) -> Self::RequestFuture {
        if params.len() < 1 {
            return Box::new(future::err("Please specify language".into()));
        }
        let language = val_to_struct::<PathBuf>(&params[0]);
        let params = &params[1..];
        match method {
            "index" => Box::new(future::lazy(|| future::ok(struct_to_val(index(&ALL_CORPORA, language))))),
            "view" => {
                    let corpus = val_to_struct::<PathBuf>(&params[0]);
                    let p = val_to_struct::<PathBuf>(&params[1]);
                    Box::new(future::lazy(|| future::ok(struct_to_val(view(&ALL_CORPORA, language, corpus, p)))))
                },
            "search" => {
                    let search_these = val_to_struct::<Vec<PathBuf>>(&params[0]);
                    let args = val_to_struct::<CorpusSearcherArgs>(&params[1]);
                    let params = SearchParams(search_these, CorpusSearcher::from(args));
                    println!("doing the RPC");
                    Box::new(future::lazy(|| future::ok(struct_to_val(search(&ALL_CORPORA, language, params)))))
                },
            _ => Box::new(future::err("Rip".into())),
        }
    }

    /// Define how the server handle notifications. This server just prints the method in the
    /// console.
    fn handle_notification(&mut self, method: &str, _: &[Value]) {
        println!("{}", method);
    }
}

pub fn main() {
    lazy_static::initialize(&ALL_CORPORA);
    // initialise logger
    Logger::with_str("info")
        .log_to_file()
        .append()
        .duplicate_to_stderr(flexi_logger::Duplicate::All)
        .directory("logs")
        .suppress_timestamp()
        .format(flexi_logger::detailed_format)
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
    let addr: SocketAddr = "127.0.0.1:54321".parse().unwrap();
    // Create a listener to listen for incoming TCP connections.
    let server = TcpListener::bind(&addr)
        .unwrap()
        .incoming()
        // Each time the listener finds a new connection, start up a server to handle it.
        .map_err(|e| info!("error on TcpListener: {}", e))
        .for_each(move |stream| {
            info!("new connection {:?}", stream);
            info!("spawning a new Server");
            /// Important! The server must be spawned in the background! Otherwise, our server will
            /// wait for each connection to be processed before accepting a new one.
            tokio::spawn(serve(stream, Server).map_err(|e| info!("server error {}", e)))
        });

    // Run the server on the tokio event loop. This is blocking. Press ^C to stop
    tokio::run(server);

}

