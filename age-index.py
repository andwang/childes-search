#!/usr/bin/python3

# This file creates a SQLite database that stores speaker information for each
# conversation. It basically scrapes the @ID headers, parses them, and makes
# a SQL database.
#
# The goal is not to reduce a conversation to a database of utterances. Rather
# we are trying to make the metadata organised and accessible so you know where
# to find which conversation.

from pathlib import Path
import sqlite3 as sql
import re

p = Path("/opt/corpora")

def walk_files(p, func: "Fn(p: Path)"):
    # dfs in 20 seconds
    for child in p.iterdir():
        if child.is_dir():
            walk_files(child, func)
        elif child.is_file():
            if child.suffix == '.cha':
                func(child)

# TODO: port to libchildes instead of cloning functionality

def add_to_db(conn, cha_file: Path):
    for line in cha_file.open():
        if line.startswith("@ID:"):
            header, _, content = line.partition(":")
            language,corpus,code,age,sex,group,SES,role,education,custom,*_ = content.strip().split("|")
            age = sum(a * b for (a, b) in zip([int(x or 0) for x in re.split(r";|\.", age, 3)], [1, 1/12, 1/365]))
            conn.execute("INSERT INTO participants VALUES(?, ?, ?, ?, ?)", [str(cha_file), code or None, age or None, sex or None, role or None])

if __name__ == "__main__":
    with sql.connect("participants.db") as conn:
        conn.execute("DROP TABLE IF EXISTS participants")
        conn.execute(
            """CREATE TABLE participants(
                file TEXT NOT NULL,
                code TEXT,
                age REAL,
                sex TEXT,
                role TEXT
            )"""
        )
        conn.commit()
        walk_files(p, lambda p: add_to_db(conn, p))
        conn.commit()
# TODO: conn.close() here?
