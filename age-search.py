#!/usr/bin/python3

import sys
import sqlite3 as sql

_, lo, hi, *_ = sys.argv

lo = float(lo)
hi = float(hi)

with sql.connect("participants.db") as conn:
    for f, in conn.execute("SELECT file FROM participants WHERE age IS NOT NULL AND age BETWEEN ? and ? GROUP BY file", [lo, hi]):
        print(f)
