# childes-search

Childes-search enables searching childes.talkbank.org transcripts with
regular expressions.

It was created for Ken Wexler's MIT 9.59 Child Language Acquisition
class in 2002, and has come to be in use by others, including Tom
Roeper's class at UMass Amherst.  That original version used to run at
  http://people.csail.mit.edu/gremio/passives.cgi

Without messing with that, this repository exists to move this tool
into the open-source world, with the aim of rewriting it in Rust and
extending its functionality. We got the first step down, but let's see
if the other part happens!

## Setup

Clone the repo.

Run `bash fetch-corpora.sh` as a user who can write to `/opt/`.

Run `python age-index.py`.

Install [codesearch](https://github.com/google/codesearch), and run `cindex /opt/corpora/`.

The search tool assumes that `csearch` is in the PATH, so put it there first.[1] Then, run `cargo run`. This will start the corpus server for RPC.

In a different window, run `python childes_frontend` to start the front-facing website.

Visit http://localhost:8000 and be on your way.


[1] e.g. `PATH="$HOME/go/bin:$PATH" cargo run`.

### Static content

When you're setting up, be warned! The default setup is currently designed to work fully out of the box, nothing else required. However, as a result, it means that we are delivering static content inefficiently, as we're unable to do anything remotely sophisticated like caching.

If you want, you can use a real web server like [nginx](https://nginx.org) or [Apache](https://httpd.apache.org) to statically serve `GET /static/childes-search/...` requests, and reverse-proxy requests to `/childes-search/` to CHILDES Search, which will prevent the default static file handler from being used.

Additionally, the default setup assumes that static content is just another directory, but we realise that it's not always that simple in the real world. If you have a cdn, just rewrite all of the urls in `templates/` to point to the cdn instead, and you'll have to deal with the static content yourself.
