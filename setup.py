#!/usr/bin/python3

from setuptools import setup

setup(
    name="childes-frontend",
    version="1.0",
    packages=["childes_frontend"],
    include_package_data=True,
    # rust extensions are not zip safe, just like C-extensions.
    zip_safe=False,
)
