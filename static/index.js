function checkAllSubcorpuses(checkbox) {
    var theseboxes = document.getElementsByClassName(checkbox.id);
    for (var i = 0; i < theseboxes.length; ++i)
        theseboxes[i].checked = checkbox.checked;
}


/* show the new node and hide the old one */
function toggleSearchType(newNode, oldNode) {
    newNode.style.display = '';
    oldNode.style.display = 'none';
    oldNode.getElementsByTagName("input")[0].value = '';
    return false;
}

/* hide the displayed advanced search options and show the hidden ones. */
function toggleAdvancedOptions() {
    var showThese = Array.from(document.getElementsByClassName('advanced-search-option-hide'));
    var hideThese = Array.from(document.getElementsByClassName('advanced-search-option-show'));
    console.log("showing", showThese);
    console.log("hiding", hideThese);
    for (var i = 0; i < showThese.length; ++i) {
        showThese[i].classList.add('advanced-search-option-show');
        var inputs = showThese[i].getElementsByTagName("input");
        for (var j = 0; j < inputs.length; ++j) {
            inputs[j].disabled = false;
        }
    }
    for (var i = 0; i < hideThese.length; ++i) {
        hideThese[i].classList.remove('advanced-search-option-show');
        var inputs = hideThese[i].getElementsByTagName("input");
        for (var j = 0; j < inputs.length; ++j) {
            inputs[j].disabled = true;
        }
    }
}
