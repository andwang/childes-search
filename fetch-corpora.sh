#!/bin/bash -x

# original full list
# language_paths=( Eng-NA Eng-UK Eng-AAVE Spanish German French Celtic/Irish Celtic/Welsh Slavic/Croatian Slavic/Polish Slavic/Russian Slavic/Serbian Slavic/Slovenian Japanese )


language_paths=( Eng-NA Eng-UK )

for language_path in "${language_paths[@]}"; do
    language="$(basename $language_path)"
    mkdir -p /opt/corpora/"$language"
    pushd /opt/corpora/"$language"
    curl https://childes.talkbank.org/access/"$language_path"/ | python3 -c 'import sys, bs4; soup = bs4.BeautifulSoup(sys.stdin, "lxml");[print(link["href"]) for link in soup.find_all("a") if link["href"].endswith(".html")]' |  while read corpus; do
        case $corpus in
            http*)
                corpus_url="${corpus%.html}".zip
                ;;
            *)
                corpus_url=https://childes.talkbank.org/data/"$language_path"/"${corpus%.html}".zip
                ;;
        esac
        wget -nc "$corpus_url"
        echo A | unzip "$(basename  $corpus_url)" &
    done
    popd
done

wait
echo 'Done!'
# English is a special case
mkdir /opt/corpora/English/
cp -r /opt/corpora/Eng-UK/* /opt/corpora/English
cp -r /opt/corpora/Eng-NA/* /opt/corpora/English
